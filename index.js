// Configurations
var config = require('./config');

// Create Express web app
var app = require('./webapp');


/**
 * Exit application
 **/
function exit() {
    process.exit();
}

process.on('SIGINT', exit);

// Start the web service
app.listen(config.port, function () {
    console.log('Server listening on port ' + config.port)
});


