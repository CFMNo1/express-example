// Map routes to controller functions
var fs = require('fs');
var path = require('path');

var config = require('.././config');

var bcrypt = require('bcrypt');

var session = require('express-session');


/**
 * Register handlers
 **/
module.exports.loadMainCode = function(router) {
    /**
     * GET /
     **/
    router.get('/', function (req, res) {
        res.status(200);
        res.render('pages/index', {
            title: "Shadowfeed"
        });
    });

}
