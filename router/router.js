/**
 * Load all our routers
 **/
var main = require('./main');

module.exports = function(router) {
    /**
     * Load all the code related to our routers
     **/
    main.loadMainCode(router);
}
